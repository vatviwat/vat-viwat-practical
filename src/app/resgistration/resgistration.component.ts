import { Component, DoCheck, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-resgistration',
  templateUrl: './resgistration.component.html',
  styleUrls: ['./resgistration.component.css']
})
export class ResgistrationComponent implements OnInit, DoCheck {

  registerForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) { }
  ngDoCheck(): void {
    console.log(this.registerForm.value);
    
  }
 

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required,Validators.maxLength(32)]],
      lastName: ['', [Validators.required,Validators.maxLength(32)]],
      email: ['', [Validators.required, Validators.email]],
      areaCode: ['', [Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
      phoneNumber: ['', [Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
      address: ['', [Validators.required]],
      address2: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      zipCode: ['', [Validators.required,Validators.pattern(/\-?\d*\.?\d{1,2}/)]],
      country: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(128)]],
      confirmPassword: ['', [Validators.required,Validators.minLength(8),Validators.maxLength(128)]],
      month: ['', [Validators.required]],
      day: ['', [Validators.required]],
      year: ['', [Validators.required]],
      membership: [false, [Validators.requiredTrue]],
      privacy: [false, [Validators.requiredTrue]],
    },{validator: MustMatch('password', 'confirmPassword')});
    
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    alert(JSON.stringify(this.registerForm.value, null,4));
  }

}

export function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          return;
      }

      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}
